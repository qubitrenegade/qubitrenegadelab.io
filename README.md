# qubitrenegade.gitlab.io

[qubitrenegade.gitlab.io](http://qubitrenegade.gitlab.io) repo

First time:

```bash
docker run -it -d --name jekyll -p 4000:4000 -p 35729:35729 --mount type=bind,source="${HOME}"/.ssh,target=/root/.ssh --mount type=bind,source="${HOME}"/qubitrenegade.gitlab.io,target=/jekyll ruby:2.5
```

Then you can:

```bash
docker start|stop jekyll
docker exec -it jekyll bash
```

Once in the container:

```baash
cd /jekyll
bundle install
bundle exec jekyll serve -l -H 0.0.0.0 --draft --future
```

Then you can visit the blog at `http://<docker host ip>:4000`.  Pages update on file save.  With the `-l` flag the page will auto reload in the browser.

## Spelling

```bash
docker run -it -d --name mdspell --mount type=bind,source=/home/ubuntu/.ssh,target=/root/.ssh --mount type=bind,source="${HOME}"/qubitrenegade.gitlab.io,target=/jekyll node:latest
docker exec mdspell npm install -g write-good markdown-spellcheck
```

```bash
# Check everything
docker exec -w /jekyll mdspell mdspell --report --ignore-acronyms --ignore-numbers --en-us **/*.md
docker exec -w /jekyll mdspell write-good **/*.md

# Check "public facing" pages
docker exec -w /jekyll mdspell mdspell --report --ignore-acronyms --ignore-numbers --en-us index.md notes.md _posts/*.md notes/*.md
docker exec -w /jekyll mdspell write-good index.md notes.md _posts/*.md notes/*.md
```
