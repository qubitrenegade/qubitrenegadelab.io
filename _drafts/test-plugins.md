---
layout: post
title:  "Test Plugins: Please Ignore"
date:   2000-02-01 12:13:57 -0700
categories: testing tppi
---

#

{% github_sample_ref /qubitrenegade/certbot-exec/blob/master/resources/certbot_cmd.rb %}
{% highlight ruby %}
{% github_sample /qubitrenegade/certbot-exec/blob/master/resources/certbot_cmd.rb 9 19 %}
{% endhighlight %}

Try including via "script":

{% highlight ruby %}
<script src="https://gist.githubusercontent.com/qubitrenegade/2ff70cc63346a09c2374262e1075564c/raw/3c2d08dcac19f5d5cfbee2453b656417bbb8edd8/vfio-pci-override.sh"> </script>
{% endhighlight %}
