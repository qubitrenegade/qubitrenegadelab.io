---
layout: post
title:  "Migrating to GitLab"
date:   2019-09-08 00:00:29 -0700
Categories: Misc
---

After a bit of debate, I've moved the blog to [GitLab Pages](https://about.gitlab.com/product/pages/).  This has nothing to do with the change in ownership at GitHub and everything to do with wanting more control over the build process.  Unfortunately, GH Pages only supports [a specific list](https://pages.github.com/versions/) of Jekyll plugins.

The blog may end up back on GitHub as the idea is "as little work as possible"... but for now we're trying the GitLab experiment.

I'll probably have more to say on this topic at a later point.  This mostly serves to validate that our CDN is working.

## Initial impressions (12 hours after initial post)

Well, out of the gate it was easy to implement automated spellcheck, however, redirecting the default `gitlab.io` domain to [qubitrenegade.com](https://qubitrenegade.com) was a bit tricky...  It [looks like](https://gitlab.com/gitlab-org/gitlab-ce/issues/42949#note_206473489) this may be a feature coming soon, which is so far the only thing missing for feature parity with GitHub Pages.
