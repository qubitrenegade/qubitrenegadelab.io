// from https://gitlab.com/gitlab-org/gitlab-ee/issues/302#note_91950663
if (location.hostname.search(/qubitrenegade\.gitlab\.io/) === 0) {
  var path;
  if (location.pathname.lastIndexOf("/", 0) === 0) {
    path = location.pathname;
  }
  else {
    path = "/" + location.pathname;
  }
  location.href = "https://qubitrenegade.com" + path + location.search + location.hash;
}
